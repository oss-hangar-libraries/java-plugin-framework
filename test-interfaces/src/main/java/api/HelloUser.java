package api;

import lombok.RequiredArgsConstructor;

/**
 * This is for testing plugins that extend classes
 * and that have constructors that take arguments
 */
@RequiredArgsConstructor
public abstract class HelloUser {
    protected final String firstName;
    protected final String lastName;

    public abstract void printHello();
}
