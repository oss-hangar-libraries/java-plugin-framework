# Java Plugin Framework
## Description
This is a plugin framework that allows its users to load plugin JAR files at runtime. It is a rework of the older
[Plugin Loader](https://gitlab.com/marvinh/plugin-system-for-java) library from marvinh. It now allows to inject 
constructor parameters while the instantiation of the plugin.

## How to use
Just add the artifact to the dependencies section of your projects pom.xml:
```
<dependency>
    <<groupId>de.osshangar</groupId>
    <artifactId>java-plugin-framework</artifactId>
    <version>1.0</version>
</dependency>
```
Now within your code create an instance of the Plugin class, 
providing the class of the interface that the plugin has to have
implemented as constructor parameter:
```
Plugin loader = new Plugin<>(PluginInterface.class);
```
In the given example, the plugin to load must extend/implement
the PluginInterface class.

Now that the loader instance has been set up, you now create a
JarInputStream and provide it to the load() method of the created
loader instance:
```
PluginInterface pluginInstance = loader.load(jarInputStream);
```
Now you can call the functions specified by PluginInterface and
implemented by the plugin from your projects Java code.

If you need to provide arguments to the Constructor of the plugin
while its instantiation, you can also call the second load() method
and provide it with an object of type Arguments. You can build this
object by using the builder method the Arguments class provide:
```
Class<?>[] constructorSignature = new Class[]{ FirstParameter.class, NextParameter.class };
Object[] constructorArguments = new Object[]{ new FirstParameter("first"), new NextParameter("next")};

PluginInterface pluginInstance = loader.load(jarInputStream, Arguments.builder()
                .constructor(constructorSignature)
                .arguments(constructorArguments)
                .build());
```

## How to build
### Building the artifact
To build the artifacts package just run
```
mvn clean package
```

### Rebuilding the test plugins
To build the test plugins, that are used by the cucumber
tests, you first need to build the test-interface package:
```
mvn -f test-interfaces clean install
```

Now build the plugins that you need for the test:
```
mvn -f test-plugin-calculator clean package
mvn -f test-plugin-hello-user clean package
mvn -f invalid-test-plugin-no-service-file clean package
mvn -f invalid-test-plugin-wrong-interface clean package
mvn -f invalid-test-plugin-no-interface clean package
```
This will build the test plugins and deploy them
to the test resources folder of the artifact, so that
the tests can find them.

Now you can test the artifact package:
```
mvn verify
```

