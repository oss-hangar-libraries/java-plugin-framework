package hellouser;

/**
 * This dependency class has been introduced to test if
 * the class loader is also able to find dependencies within the
 * JAR package of a loaded plugin.
 */
public class ReallyImportantDependency {
    public String formatOutput(String firstName, String lastName){
        return String.format("Hello %s %s.", firstName, lastName);
    }
}
