package de.osshangar.plugin.exception;

public class FileFormatException extends Exception{
    public FileFormatException(String message) {
        super(message);
    }
}
