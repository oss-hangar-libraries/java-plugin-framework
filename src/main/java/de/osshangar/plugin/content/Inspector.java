package de.osshangar.plugin.content;

import de.osshangar.plugin.exception.FileFormatException;
import lombok.Getter;
import lombok.NonNull;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;
import java.util.stream.Collectors;

@Getter
public class Inspector {
    // the name of the class that is to be loaded as a plugin
    private String pluginClassName;

    // the name of the file (it has the name of the interface the plugin implements)
    // that specifies the class to be loaded as a plugin
    private String pluginClassSpecificationFile;

    // stores all entries of the plugin jar file with their name
    private final Map<String, byte[]> entries = new HashMap<>();

    // contains all entries of the plugin jar that are class files with their name
    private final Map<String, byte[]> classFiles = new HashMap<>();

    private void setClassFiles(){
        entries.entrySet().stream().filter(file -> file.getKey().endsWith(".class")).forEach(file -> {
                    String className = file.getKey()
                            .replace(".class", "")
                            .replace("/", ".");
                    classFiles.put(className, file.getValue());
                });
    }

    private void setPluginInterface(@NonNull Class<?> interfaceClass) throws FileNotFoundException {
        String pluginClassSpecificationFile = String.format("META-INF/services/%s", interfaceClass.getCanonicalName());

        if (!entries.containsKey(pluginClassSpecificationFile)){
            throw new FileNotFoundException(pluginClassSpecificationFile);
        }

        this.pluginClassSpecificationFile = pluginClassSpecificationFile;
    }

    private void setPluginClassName() throws FileFormatException {
        assert pluginClassSpecificationFile != null;
        assert entries.containsKey(pluginClassSpecificationFile);

        List<String> pluginClassSpecificationFileLines = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(entries.get(pluginClassSpecificationFile)))).lines().collect(Collectors.toList());
        if (pluginClassSpecificationFileLines.isEmpty()){
            throw new FileFormatException(String.format("File must contain the name of the class to be loaded as plugin: %s", pluginClassSpecificationFile));
        }
        if (pluginClassSpecificationFileLines.size() > 1){
            throw new FileFormatException(String.format("File must not specify multiple classes to be loaded as plugins: %s", pluginClassSpecificationFile));
        }
        String pluginClassName = pluginClassSpecificationFileLines.get(0).trim();
        if (pluginClassName.isEmpty()){
            throw new FileFormatException(String.format("File must contain the name of the class to be loaded as plugin: %s", pluginClassSpecificationFile));
        }
        this.pluginClassName = pluginClassName;
    }

    public static Inspector inspect(@NonNull JarInputStream jarInputStream, @NonNull Class<?> interfaceClass) throws IOException, FileFormatException {
        Inspector inspector = new Inspector();

        JarEntry jarEntry = jarInputStream.getNextJarEntry();
        while (jarEntry != null){
            if (!jarEntry.isDirectory()){
                inspector.entries.put(jarEntry.getName(), IOUtils.toByteArray(jarInputStream));
            }
            jarEntry = jarInputStream.getNextJarEntry();
        }

        inspector.setClassFiles();
        inspector.setPluginInterface(interfaceClass);
        inspector.setPluginClassName();

        return inspector;
    }
}
