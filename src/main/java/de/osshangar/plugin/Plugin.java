package de.osshangar.plugin;

import de.osshangar.plugin.classloader.PluginClassLoader;
import de.osshangar.plugin.content.Inspector;
import de.osshangar.plugin.exception.FileFormatException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.jar.JarInputStream;

/**
 * This class loads the plugins
 * @param <T> type of the object created
 * @param <C> the interface class that the plugin has to implement
 */
@RequiredArgsConstructor
public class Plugin<T, C extends Class<T>> {
    @NonNull
    private final C interfaceClass;

    /**
     * This loads the plugin from the provided JarInputStream.
     * @param jarInputStream The input stream to a JAR package to load the plugin from
     * @return The instantiated plugin instance
     * @throws FileFormatException when the structure of the plugin JAR is wrong
     * @throws IOException when an I/O operation fails
     * @throws ClassNotFoundException when the PluginClassLoader does not find a requested class
     * @throws InvocationTargetException when the provided constructor arguments does not match the signature of the requested constructor
     * @throws NoSuchMethodException when then plugin does not have a constructor that matches the requested signature
     * @throws InstantiationException when the class of the plugin which implements the required plugin interface is an abstract class
     * @throws IllegalAccessException when the requested constructor of the plugin is inaccessible due to an enforced Java language access control
     */
    public T load(JarInputStream jarInputStream) throws FileFormatException, IOException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return load(jarInputStream, null);
    }

    /**
     * This loads the plugin from the provided JarInputStream and provides the given arguments to its
     * constructor
     * @param jarInputStream The input stream to a JAR package to load the plugin from
     * @param constructorArguments The arguments to be provided to the constructor of the plugin on its instantiation
     * @return The instance of the loaded plugin
     * @throws FileFormatException when the structure of the plugin JAR is wrong
     * @throws IOException when an I/O operation fails
     * @throws ClassNotFoundException when the PluginClassLoader does not find a requested class
     * @throws InvocationTargetException when the provided constructor arguments does not match the signature of the requested constructor
     * @throws NoSuchMethodException when then plugin does not have a constructor that matches the requested signature
     * @throws InstantiationException when the class of the plugin which implements the required plugin interface is an abstract class
     * @throws IllegalAccessException when the requested constructor of the plugin is inaccessible due to an enforced Java language access control
     */
    public T load(JarInputStream jarInputStream, Arguments constructorArguments) throws FileFormatException, IOException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        Inspector inspector = Inspector.inspect(jarInputStream, interfaceClass);
        PluginClassLoader classLoader = new PluginClassLoader(inspector.getClassFiles());
        return createInstance(classLoader, inspector, constructorArguments);
    }

    private T createInstance(PluginClassLoader classLoader, Inspector inspector, Arguments constructorArguments) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Class<?> pluginClass = classLoader.findClass(inspector.getPluginClassName());
        if (!interfaceClass.isAssignableFrom(pluginClass)){
            throw new ClassCastException(String.format("The compiled class is no implementation or subclass of %s", interfaceClass.getCanonicalName()));
        }

        Class<?>[] signature = new Class[]{};
        Object[] arguments = new Object[]{};
        if (constructorArguments != null){
            signature = constructorArguments.getConstructor();
            arguments = constructorArguments.getArguments();
        }

        Constructor<?> constructor = pluginClass.getConstructor(signature);
        //noinspection unchecked
        return (T) constructor.newInstance(arguments);
    }
}
