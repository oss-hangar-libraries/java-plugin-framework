package de.osshangar.plugin.classloader;

import lombok.NonNull;

import java.util.Map;

public class PluginClassLoader extends ClassLoader {
    // contains all class files that are known to the class loader with their name
    private final Map<String, byte[]> classFiles;

    public PluginClassLoader(@NonNull Map<String, byte[]> classesFiles) {
        super(PluginClassLoader.class.getClassLoader());
        this.classFiles = classesFiles;
    }

    @Override
    public Class<?> findClass(@NonNull String fullQualifiedClassName) throws ClassNotFoundException {
        if (!classFiles.containsKey(fullQualifiedClassName)){
            throw new ClassNotFoundException(String.format("Did not find class '%s'", fullQualifiedClassName));
        }
        return defineClass(fullQualifiedClassName, classFiles.get(fullQualifiedClassName), 0, classFiles.get(fullQualifiedClassName).length);
    }

    @Override
    public Class<?> loadClass(String fullQualifiedClassName) throws ClassNotFoundException {
        if (classFiles.containsKey(fullQualifiedClassName)){
            return findClass(fullQualifiedClassName);
        }
        ClassLoader defaultLoader = Thread.currentThread().getContextClassLoader();
        Class<?> loadedClass = defaultLoader.loadClass(fullQualifiedClassName);
        return loadedClass;
    }

}
