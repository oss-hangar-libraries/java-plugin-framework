package de.osshangar.plugin;

import de.osshangar.plugin.exception.FileFormatException;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import lombok.NonNull;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.jar.JarInputStream;

import static org.junit.Assert.*;

public class StepDefinitions {
    private static final String pluginPath = "plugins";

    Plugin<?, ?> loader;
    Class<?> interfaceClass;

    @Given("a loader instance for {string} objects exists")
    public void createPluginLoaderInstance(@NonNull String classname) throws ClassNotFoundException {
        interfaceClass = Class.forName(classname);
        loader = new Plugin<>(interfaceClass);
    }

    @Then("successfully load the plugin JAR file {string} with no arguments")
    public void loadPluginWithoutArguments(@NonNull String filename) throws IOException, FileFormatException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        assert loader != null;

        InputStream inputStream;
        Object instance;

        //load using no-args load function
        inputStream = ClassLoader.getSystemResourceAsStream(String.format("%s/%s", pluginPath, filename));
        assertNotNull(inputStream);
        instance = loader.load(new JarInputStream(inputStream));
        assertNotNull(instance);
        assertTrue(interfaceClass.isAssignableFrom(instance.getClass()));

        //load using null as constructor arguments
        inputStream = ClassLoader.getSystemResourceAsStream(String.format("%s/%s", pluginPath, filename));
        assertNotNull(inputStream);
        instance = loader.load(new JarInputStream(inputStream), null);
        assertNotNull(instance);
        assertTrue(interfaceClass.isAssignableFrom(instance.getClass()));

        //load using empty array as constructor arguments
        inputStream = ClassLoader.getSystemResourceAsStream(String.format("%s/%s", pluginPath, filename));
        assertNotNull(inputStream);
        instance = loader.load(new JarInputStream(inputStream), Arguments.builder()
                .constructor(new Class[]{})
                .arguments(new Object[]{})
                .build());
        assertNotNull(instance);
        assertTrue(interfaceClass.isAssignableFrom(instance.getClass()));
    }

    @Then("successfully load the plugin JAR file {string} with signature {string} and arguments {string}")
    public void loadPluginWithArguments(@NonNull String filename, @NonNull String signature, @NonNull String values) throws IOException, ClassNotFoundException, FileFormatException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        assert loader != null;

        InputStream inputStream = ClassLoader.getSystemResourceAsStream(String.format("%s/%s", pluginPath, filename));
        assertNotNull(inputStream);

        Object instance = loader.load(new JarInputStream(inputStream), Arguments.builder()
                .constructor(SignatureParser.parseSignature(signature))
                .arguments(SignatureParser.parseArguments(values))
                .build());
        assertNotNull(instance);
        assertTrue(interfaceClass.isAssignableFrom(instance.getClass()));
    }

    @Then("fail loading the plugin JAR file {string} with no arguments throwing {string}")
    public void failLoadPluginWithNoArguments(@NonNull String filename, @NonNull String exception) throws ClassNotFoundException {
        assert loader != null;

        @SuppressWarnings("unchecked") Class<? extends Throwable> expectedException = (Class<? extends Throwable>) Class.forName(exception);

        InputStream inputStream0 = ClassLoader.getSystemResourceAsStream(String.format("%s/%s", pluginPath, filename));
        assertNotNull(inputStream0);
        assertThrows(expectedException, () -> loader.load(new JarInputStream(inputStream0)));

        InputStream inputStream1 = ClassLoader.getSystemResourceAsStream(String.format("%s/%s", pluginPath, filename));
        assertNotNull(inputStream1);
        assertThrows(expectedException, () -> loader.load(new JarInputStream(inputStream1), null));

        InputStream inputStream2 = ClassLoader.getSystemResourceAsStream(String.format("%s/%s", pluginPath, filename));
        assertNotNull(inputStream2);
        assertThrows(expectedException, () -> loader.load(new JarInputStream(inputStream2), Arguments.builder()
                .constructor(new Class[]{})
                .arguments(new Object[]{})
                .build()));
    }

    @Then("fail loading the plugin JAR file {string} with signature {string} and arguments {string} throwing {string}")
    public void failLoadPluginWithArguments(@NonNull String filename, @NonNull String signature, @NonNull String values, @NonNull String exception) throws ClassNotFoundException, IOException {
        assert loader != null;

        @SuppressWarnings("unchecked") Class<? extends Throwable> expectedException = (Class<? extends Throwable>) Class.forName(exception);

        InputStream inputStream = ClassLoader.getSystemResourceAsStream(String.format("%s/%s", pluginPath, filename));
        assertNotNull(inputStream);

        assertThrows(expectedException, () -> loader.load(new JarInputStream(inputStream), Arguments.builder()
                .constructor(SignatureParser.parseSignature(signature))
                .arguments(SignatureParser.parseArguments(values))
                .build()));
    }
}
