Feature: Load invalid plugin

  Rule: Invalid plugin files should throw an Exception
    Scenario Outline: Load invalid plugin file with no arguments
      Given a loader instance for "<classname>" objects exists
      Then fail loading the plugin JAR file "<filename>" with no arguments throwing "<exception>"

      Examples:
        | filename                                    | classname      | exception                        |
        | invalid-test-plugin-no-service-file-1.0.jar | api.Calculator | java.io.FileNotFoundException    |
        | invalid-test-plugin-wrong-interface-1.0.jar | api.Calculator | java.lang.ClassNotFoundException |
        | invalid-test-plugin-no-interface-1.0.jar    | api.Calculator | java.lang.ClassCastException     |

    Scenario Outline: Load invalid plugin file with arguments
      Given a loader instance for "<classname>" objects exists
      Then fail loading the plugin JAR file "<filename>" with signature "<signature>" and arguments "<values>" throwing "<exception>"

      Examples:
        | filename                                    | classname      | exception                        | signature                          | values                                           |
        | invalid-test-plugin-no-service-file-1.0.jar | api.Calculator | java.io.FileNotFoundException    | java.lang.String, java.lang.String | java.lang.String:Marvin, java.lang.String:Haagen |
        | invalid-test-plugin-wrong-interface-1.0.jar | api.Calculator | java.lang.ClassNotFoundException | java.lang.String, java.lang.String | java.lang.String:Marvin, java.lang.String:Haagen |
        | invalid-test-plugin-no-interface-1.0.jar    | api.Calculator | java.lang.ClassCastException     | java.lang.String, java.lang.String | java.lang.String:Marvin, java.lang.String:Haagen |


  Rule: Invalid plugin files should throw an Exception even if they are
  loaded multiple times with the same loader instance

    Scenario Outline: Load invalid plugin multiple times with same instance and no arguments
      Given a loader instance for "<classname>" objects exists
      Then fail loading the plugin JAR file "<filename>" with no arguments throwing "<exception>"
      And fail loading the plugin JAR file "<filename>" with no arguments throwing "<exception>"
      And fail loading the plugin JAR file "<filename>" with no arguments throwing "<exception>"

      Examples:
        | filename                                    | classname      | exception                        |
        | invalid-test-plugin-no-service-file-1.0.jar | api.Calculator | java.io.FileNotFoundException    |
        | invalid-test-plugin-wrong-interface-1.0.jar | api.Calculator | java.lang.ClassNotFoundException |
        | invalid-test-plugin-no-interface-1.0.jar    | api.Calculator | java.lang.ClassCastException     |


  Rule: Invalid plugin files should throw an Exception even if they are
  loaded multiple times with the different instances

    Scenario Outline: Load valid plugin multiple times with different instances
      Given a loader instance for "<classname>" objects exists
      Then fail loading the plugin JAR file "<filename>" with no arguments throwing "<exception>"
      Then a loader instance for "<classname>" objects exists
      And fail loading the plugin JAR file "<filename>" with no arguments throwing "<exception>"
      Then a loader instance for "<classname>" objects exists
      And fail loading the plugin JAR file "<filename>" with no arguments throwing "<exception>"

      Examples:
        | filename                                    | classname      | exception                        |
        | invalid-test-plugin-no-service-file-1.0.jar | api.Calculator | java.io.FileNotFoundException    |
        | invalid-test-plugin-wrong-interface-1.0.jar | api.Calculator | java.lang.ClassNotFoundException |
        | invalid-test-plugin-no-interface-1.0.jar    | api.Calculator | java.lang.ClassCastException     |