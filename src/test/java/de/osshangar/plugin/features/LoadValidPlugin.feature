Feature: Load valid plugin

  Rule: Valid plugin files that match the specified interfaces should be instantiated correctly

    Scenario Outline: Load valid plugin file with no arguments
      Given a loader instance for "<classname>" objects exists
      Then successfully load the plugin JAR file "<filename>" with no arguments

      Examples:
        | filename                       | classname      |
        | test-plugin-calculator-1.0.jar | api.Calculator |

    Scenario Outline: Load valid plugin file with arguments
      Given a loader instance for "<classname>" objects exists
      Then successfully load the plugin JAR file "<filename>" with signature "<signature>" and arguments "<values>"

      Examples:
        | filename                       | classname     | signature                          | values                                           |
        | test-plugin-hello-user-1.0.jar | api.HelloUser | java.lang.String, java.lang.String | java.lang.String:Marvin, java.lang.String:Haagen |


  Rule: Loading valid plugin files should throw an Exception, when the constructor signature of the arguments not match

    Scenario Outline: Load valid plugin with invalid constructor
      Given a loader instance for "<classname>" objects exists
      Then fail loading the plugin JAR file "<filename>" with signature "<signature>" and arguments "<values>" throwing "<exception>"

      Examples:
        | filename                       | classname     | exception                       | signature                                            | values                                                                   |
        | test-plugin-hello-user-1.0.jar | api.HelloUser | java.lang.NoSuchMethodException | java.lang.String                                     | java.lang.String:Haagen                                                  |
        | test-plugin-hello-user-1.0.jar | api.HelloUser | java.lang.NoSuchMethodException | java.lang.String, java.lang.Integer                  | java.lang.String:Haagen, java.lang.Integer:42                            |
        | test-plugin-hello-user-1.0.jar | api.HelloUser | java.lang.NoSuchMethodException | java.lang.String, java.lang.String, java.lang.String | java.lang.String:Marvin, java.lang.String:Haagen, java.lang.String:B.Sc. |
        | test-plugin-hello-user-1.0.jar | api.HelloUser | java.lang.NoSuchMethodException |                                                      |                                                                          |


  Rule: Loading valid plugin files should throw an Exception, when the provided arguement values does not match the requested constructor

    Scenario Outline: Load valid plugin with invalid arguments list
      Given a loader instance for "<classname>" objects exists
      Then fail loading the plugin JAR file "<filename>" with signature "<signature>" and arguments "<values>" throwing "<exception>"

      Examples:
        | filename                       | classname     | exception                          | signature                          | values                                                                   |
        | test-plugin-hello-user-1.0.jar | api.HelloUser | java.lang.IllegalArgumentException | java.lang.String, java.lang.String | java.lang.String:Haagen                                                  |
        | test-plugin-hello-user-1.0.jar | api.HelloUser | java.lang.IllegalArgumentException | java.lang.String, java.lang.String | java.lang.String:Haagen, java.lang.Integer:42                            |
        | test-plugin-hello-user-1.0.jar | api.HelloUser | java.lang.IllegalArgumentException | java.lang.String, java.lang.String | java.lang.String:Marvin, java.lang.String:Haagen, java.lang.String:B.Sc. |
        | test-plugin-hello-user-1.0.jar | api.HelloUser | java.lang.IllegalArgumentException | java.lang.String, java.lang.String |                                                                          |


  Rule: Valid plugin files that match the specified interfaces should be instantiated correctly even if they are
  loaded multiple times with the same loader instance

    Scenario Outline: Load valid plugin multiple times with same instance and no arguments
      Given a loader instance for "<classname>" objects exists
      Then successfully load the plugin JAR file "<filename>" with no arguments
      And successfully load the plugin JAR file "<filename>" with no arguments
      And successfully load the plugin JAR file "<filename>" with no arguments

      Examples:
        | filename                       | classname      |
        | test-plugin-calculator-1.0.jar | api.Calculator |


  Rule: Valid plugin files that match the specified interfaces should be instantiated correctly even if they are
  loaded multiple times with the different instances

    Scenario Outline: Load valid plugin multiple times with different instances
      Given a loader instance for "<classname>" objects exists
      Then successfully load the plugin JAR file "<filename>" with no arguments
      And a loader instance for "<classname>" objects exists
      Then successfully load the plugin JAR file "<filename>" with no arguments
      And a loader instance for "<classname>" objects exists
      Then successfully load the plugin JAR file "<filename>" with no arguments

      Examples:
        | filename                       | classname      |
        | test-plugin-calculator-1.0.jar | api.Calculator |

