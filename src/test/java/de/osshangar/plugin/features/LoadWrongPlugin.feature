Feature: Load wrong plugin

  Rule: Valid plugin files that does not match the specified interface should throw an Exception

    Scenario Outline: Load wrong plugin file with no arguments
      Given a loader instance for "<classname>" objects exists
      Then fail loading the plugin JAR file "<filename>" with no arguments throwing "<exception>"

      Examples:
        | filename                       | classname     | exception                     |
        | test-plugin-calculator-1.0.jar | api.HelloUser | java.io.FileNotFoundException |

    Scenario Outline: Load wrong plugin file with arguments
      Given a loader instance for "<classname>" objects exists
      Then fail loading the plugin JAR file "<filename>" with signature "<signature>" and arguments "<values>" throwing "<exception>"

      Examples:
        | filename                       | classname      | exception                     | signature                          | values                                           |
        | test-plugin-hello-user-1.0.jar | api.Calculator | java.io.FileNotFoundException | java.lang.String, java.lang.String | java.lang.String:Marvin, java.lang.String:Haagen |


  Rule: Valid plugin files that does not match the specified interface should throw an Exception even if they are
  loaded multiple times with the same loader instance

    Scenario Outline: Load wrong plugin file multiple times with same instance
      Given a loader instance for "<classname>" objects exists
      Then fail loading the plugin JAR file "<filename>" with no arguments throwing "<exception>"
      And fail loading the plugin JAR file "<filename>" with no arguments throwing "<exception>"
      And fail loading the plugin JAR file "<filename>" with no arguments throwing "<exception>"

      Examples:
        | filename                       | classname     | exception                     |
        | test-plugin-calculator-1.0.jar | api.HelloUser | java.io.FileNotFoundException |


  Rule: Valid plugin files that does not match the specified interface should throw an Exception even if they are
  loaded multiple times with the different instances

    Scenario Outline: Load wrong plugin file multiple times with different instances
      Given a loader instance for "<classname>" objects exists
      Then fail loading the plugin JAR file "<filename>" with no arguments throwing "<exception>"
      And a loader instance for "<classname>" objects exists
      Then fail loading the plugin JAR file "<filename>" with no arguments throwing "<exception>"
      And a loader instance for "<classname>" objects exists
      Then fail loading the plugin JAR file "<filename>" with no arguments throwing "<exception>"

      Examples:
        | filename                       | classname     | exception                     |
        | test-plugin-calculator-1.0.jar | api.HelloUser | java.io.FileNotFoundException |