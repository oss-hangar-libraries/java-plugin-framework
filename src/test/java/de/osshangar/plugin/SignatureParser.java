package de.osshangar.plugin;

public class SignatureParser {
    public static Class<?>[] parseSignature(String signatureString) throws ClassNotFoundException {
        if (signatureString.trim().isEmpty()){
            return new Class[0];
        }

        String[] types = signatureString.split(",");

        Class<?>[] signature = new Class[types.length];
        for(int i = 0; i < types.length; i++){
            Class<?> type = Class.forName(types[i].trim());
            signature[i] = type;
        }

        return signature;
    }

    public static Object[] parseArguments(String argumentString) throws ClassNotFoundException {
        if (argumentString.trim().isEmpty()){
            return new Class[0];
        }

        String[] args = argumentString.split(",");

        Object[] arguments = new Object[args.length];
        for (int i = 0; i < args.length; i++){
            assert args[i].contains(":");
            Class<?> type = Class.forName(args[i].split(":")[0].trim());
            arguments[i] = parseValue(type, args[i].split(":")[1].trim());
        }

        return arguments;
    }

    private static Object parseValue(Class<?> expectedClass, String valueString){
        if (expectedClass == String.class){
           return valueString;
        }
        if (expectedClass == Integer.class){
            return Integer.parseInt(valueString);
        }
        throw new IllegalArgumentException(String.format("Don't know how to parse values of type %s", expectedClass.getCanonicalName()));
    }
}
