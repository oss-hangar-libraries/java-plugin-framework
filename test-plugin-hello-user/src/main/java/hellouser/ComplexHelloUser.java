package hellouser;

import api.HelloUser;

import java.util.Date;

public class ComplexHelloUser extends HelloUser {
    public ComplexHelloUser(String firstName, String lastName) {
        super(firstName, lastName);
    }

    @Override
    public void printHello() {
        ReallyImportantDependency dependency = new ReallyImportantDependency();
        System.out.printf("%s Everything works fine with the plugin at %s%n", dependency.formatOutput(firstName, lastName), new Date());
    }
}
